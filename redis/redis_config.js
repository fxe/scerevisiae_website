var redis = require('redis');
require("redis-scanstreams")(redis);
var client = redis.createClient({host: "127.0.0.1", port: 6379});
var cronJob = require('cron').CronJob;

/*var job = new cronJob('00 00 5 * * 0', function(){
            client.flushall(function(didSucceed){ console.log('redis reset');});
            setNeo4jReactions(client);
	    setNeo4jMetabolites(client);
        }, null, true);
*/

exports.getReactions = function(req, res){
    
    client.exists("reactions", function(err, reply){
        if (reply !== 1){
            setNeo4jReactions(client);
            //setTimeout(exports.getReactions(req,res), 5000);
            console.log("sim");
            return ;
            //setNeo4jReactions(client);
        } else {
            console.log("nao");
            //client.get('reactions', function(err, reply){
            //client.flushall();   
             getNeo4jReactions(req, res, client);
            //});
        }
    });
}

exports.getMetabolites = function(req, res){
    client.exists("all-metabolites", function(err, reply){
        if (reply !== 1){
            setNeo4jMetabolites(client);
            return ;
        } else {
            getNeo4jMetabolites(req, res, client);
        }
    });
}
