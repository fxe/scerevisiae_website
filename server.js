var express = require('express'),
    path = require('path'),
    http = require('http'),
    routes = require('./routes/routes');

var app = express();

app.configure(function() {
    app.set('port', process.env.PORT || 3100);
    app.use(express.logger('dev'));
    app.use(express.bodyParser()),
    app.use(express.static(path.join(__dirname, 'public')));
});

app.get('/sgdgenes', routes.findAllGenes);
app.get('/sgdgenes/:entry', routes.findGeneByEntry);

app.get('/reactions', routes.findAllReactions);
app.get('/reactions/:model/:reaction', routes.findReactionByEntry);

app.get('/species', routes.findAllSpecies);
app.get('/species/:model/:specie', routes.findSpecieByEntry);

app.get('/metabolites', routes.findAllMetabolites);
app.get('/metabolites/:model/:metabolite', routes.findMetaboliteByEntry);

app.get('/gprs/:model/:reaction', routes.findGPRByReaction);

http.createServer(app).listen(app.get('port'), function(){
    console.log("Express server listening on port " + app.get('port'));
});
