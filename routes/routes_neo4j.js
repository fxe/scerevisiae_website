var neo4j = require('node-neo4j');
var toArray = require('stream-to-array');
var fs = require('fs');

db = new neo4j('http://localhost:7480');

var model_list = '["iIN800", ' +
       '"ymn6_06_cobra","iTO977", ' +
       '"iFF708", ' +
       '"ymn1_0","iAZ900","MODELID_222668","iLL672", ' +
       '"ymn_7_6_cobra","iMM904"] '
	   

var model_map = {
	"iIN800" : "iIN800",
	"Yeast 6": "ymn6_06_cobra",	
	"iTO977" : "iTO977",
	"iFF708" : "iFF708",
	"Yeast 1" : "ymn1_0",
	"iAZ900" : "iAZ900",
	"iND750" : "MODELID_222668",
	"iLL672" : "iLL672",
	"Yeast 7" : "ymn_7_6_cobra",
	"iMM904" : "iMM904",
	"All": "All",
	"": ""
}

exports.findGeneByEntry = function(req, res){
    var entryGene = req.params.entry;
    db.cypherQuery(
            'MATCH (s:SGD {entry: {entry}}) ' + 
            'OPTIONAL MATCH (s)--(u:Uniprot) ' + 
            'OPTIONAL MATCH (s)--(ex:Expazy) ' +
            'OPTIONAL MATCH (s)--(p:Phenotype) ' + 
            'OPTIONAL MATCH (s)--(l:Leaf)<-[:has_leaf]-(n)<-[*1..3]-(rx:ModelReaction) ' +
			'OPTIONAL MATCH (rx)-[:has_gpr]-(gpr:Root) ' +
            'RETURN { entry: s.entry, name: s.name, status: s.status, ' +
			'alias: s.alias, featureType: s.featureType, length: s.length, ' +
			'systematic_name: s.systematic_name, standardName: s.standardName, ' +
			'phenotypes: collect(DISTINCT p.key), uniprot: u.entry, expazy: ex.entry, ' +
			'gprs: collect(DISTINCT {entry: rx.entry, gpr_normalized: gpr.normalized_rule}) }',
            {
                entry: entryGene
            }, function(err, result){
                if (err){
                    return console.log(err);
                }
                res.send(result.data[0]);
            }
            
    );
};

exports.findAllGenes = function(req, res){
    var genesTableMap = {
        0: "d.entry",
        1: "d.systematic_name",
        2: "d.standardName",
        3: "d.entry"
    }
    var offset = parseInt(req.query.iDisplayStart);
    var number = parseInt(req.query.iDisplayLength);
    var searchText = req.query.sSearch;
    var sortCol = parseInt(req.query.iSortCol_0);
    var order = req.query.sSortDir_0;
    var echo = parseInt(req.query.sEcho);
    if (validateRegex(searchText))
        searchText = '(?i).*' + searchText + '.*';
    else searchText = '.*';

    db.cypherQuery(
        'MATCH (s:SGD)-[:has_gpr_gene]-(n:Leaf) ' +
        'RETURN count(DISTINCT s) as cnt',
        {}, function(err,result){    
            if (err){
                return console.log(err);
            }
            var num_genes = result.data[0];
            db.cypherQuery(
                'MATCH (gene:SGD)-[:has_gpr_gene]-() ' +
				'WITH DISTINCT gene ' +
                'WHERE gene.entry =~ {searchText} OR gene.systematic_name =~ {searchText} OR ' + 
                '      gene.standardName =~ {searchText} ' +
				'WITH count(gene) as cnt, collect(gene) as dataColl ' +
				'UNWIND dataColl as d ' +
                'RETURN {count: cnt, entry: d.entry, systematic_name: d.systematic_name, ' +
                '        name: d.standardName} ' +
                'ORDER BY ' + genesTableMap[sortCol] + ' ' + order + ' SKIP {offset} LIMIT {number}'
          , {
                searchText: searchText,
                offset: offset,
                number: number
           }, function(err, result2){
                    if (err) return console.log(err);
					var num_filtered = 0;
					if (result2.data.length > 0 ) num_filtered = result2.data[0].count; 
                    var final_result = {
                        iTotalRecords: parseInt(num_genes),
                        iTotalDisplayRecords: parseInt(num_filtered),
                        sEcho: echo,
                        aaData: result2.data
                    }
                    res.send(final_result);
                }
            );
        }
    ); 
};

setNeo4jReactions = function(redisClient){	
	db.cypherQuery(
		'MATCH (rx:ModelReaction)-[:has_reaction]-(mod:MetabolicModel), ' +
		 '(rx)-[l:left_component]->(lc:MetaboliteSpecie), ' +
		 '(rx)-[r:right_component]->(rc:MetaboliteSpecie) ' +
		'WHERE mod.entry IN ' + model_list +
		'OPTIONAL MATCH (rx)-[:has_gpr]-(gpr:Root) ' + 
		'OPTIONAL MATCH (rx)-[:has_crossreference_to]-(cr)-[:has_ec_number]-(ec) ' +
		'RETURN {pathway: rx.pathway, model: mod.entry, entry: rx.entry, ' +
		 'reactants: COLLECT(DISTINCT {compartment: lc.scmp, alias: lc.alias, specie: l.cpdEntry, stoichiometry: l.stoichiometry}), ' +
		 'products:COLLECT(DISTINCT {compartment: rc.scmp, alias: rc.alias, specie: r.cpdEntry, stoichiometry: r.stoichiometry}), ' +
		 'orientation: rx.orientation, name: rx.name, ecnumber: COLLECT(DISTINCT ec.entry), ' +
		 'gpr_rule: rx.geneRule, gpr_normalized: gpr.normalized_rule}'
		,{
		}, function(err, result){
			if (err){
				return console.log(err);
			}
			var model_set = "";
			for (var i = 0; i < result.data.length; i++){
				model_set = result.data[i].model;
				redisClient.sadd("reactions" + model_set, JSON.stringify(result.data[i]));
				redisClient.sadd("reactions", JSON.stringify(result.data[i]));
			}
            redisClient.set("all-reactions", JSON.stringify(result.data), function(err, reply){
                if (err) console.log(err);
            });
		}
	);
}

setNeo4jMetabolites = function(redisClient){
	db.cypherQuery(
		'MATCH (m:MetaboliteSpecie)-[:has_specie]-(mm:MetabolicModel) ' + 
		'WHERE (mm.entry IN ' + model_list + ' ) ' +
		'RETURN {entry: m.entry, alias: m.alias, model: mm.entry, name: m.name, compartment: m.scmp}'
		,{
		}, function(err, result){
			if (err) return console.log(err);
			redisClient.set("all-metabolites", JSON.stringify(result.data), function(err, reply){
				if (err) console.log(err);
			});
			
		}
	);
}

toCaseInsensitive = function(str){
	var sb = "";
	var strLower = str.toLowerCase();
    var upperCase;
    for (var i = 0; i < strLower.length; i++){
		var code = str.charCodeAt(i);
		if ((code >= 97) && (code <= 122)) {
			upperCase = strLower[i].toUpperCase();
			sb = sb + "[" + strLower[i] + upperCase + "]";
		} else sb = sb + str[i];
    }
	return sb;
}

getNeo4jReactions = function(req, res, redisClient){
	var reactionsTableMap = {
        0: "data.entry",
        1: "data.model",
        2: "data.entry",
        3: "data.ecnumber",
        4: "data.gpr_rule",
		5: "data.pathway",
        6: "data.name",
        7: "data.entry"
    }
  console.log('getNeo4jReactions');
	var offset = parseInt(req.query.iDisplayStart);
    var number = parseInt(req.query.iDisplayLength);
    var sortCol = parseInt(req.query.iSortCol_0);
    var order = req.query.sSortDir_0;
    var echo = parseInt(req.query.sEcho);
	var searchText = req.query.sSearch;
	var reactions = [];
	var searchModel = model_map[req.query.sSearch_1];
    var models = "iMM904";
	
	if (searchModel !== ""){
		if (searchModel == "All"){
			models = '';
		}
		else models = searchModel;
	}
	if (searchText == '') searchText = null;
	else {
		if (validateRegex(searchText))
			searchText = '*' + toCaseInsensitive(searchText) + '*';
		else searchText = '*';
	}
	if (searchText !== null || searchModel !== 'All'){
		toArray(redisClient.sscan("reactions" + models, {pattern: searchText, count : 20000}), function(err, arr){
			if (err) console.log(err);
			for (var i = 0; i < arr.length; i++)
				reactions.push(JSON.parse(arr[i]));
			var num_reactions_filtered = reactions.length;
			redisClient.scard("reactions"+models, function(err, reply){
				var num_reactions = reply;
				db.cypherQuery(
					'UNWIND {json} AS data ' +
					'WITH data ' + 
					'RETURN {pathway: data.pathway, entry: data.entry, reactants: data.reactants, ' + 
					'		 products: data.products, orientation: data.orientation, ' +
					'		 name: data.name, ecnumber: data.ecnumber, gpr_rule: data.gpr_rule, ' +
					'		 gpr_normalized: data.gpr_normalized} ' + 
					'ORDER BY ' + reactionsTableMap[sortCol] + ' ' + order + 
									' SKIP {offset} LIMIT {number}',
					{
						json: reactions,
						searchText: searchText,
						offset: offset,
						number: number,
					}, function(err, result){
						if (err) return console.log(err);
						var final_result = {
										iTotalRecords: parseInt(num_reactions),
										iTotalDisplayRecords: parseInt(num_reactions_filtered),
										sEcho: echo,
										aaData: result.data
									}
						res.send(final_result);	
					}
				);				
			});
			
			
		});
	} else {
		redisClient.get("all-reactions", function(err, arr){
			redisClient.scard("reactions"+models, function(err, reply){

				var num_reactions = reply;
				var num_reactions_filtered = reply;
				reactions = JSON.parse(arr);

				db.cypherQuery(
					'UNWIND {json} AS data ' +
					'WITH data ' + 
					'RETURN {pathway: data.pathway, entry: data.entry, reactants: data.reactants, ' + 
					'		 products: data.products, orientation: data.orientation, ' +
					'		 name: data.name, ecnumber: data.ecnumber, gpr_rule: data.gpr_rule, ' +
					'		 gpr_normalized: data.gpr_normalized} ' + 
					'ORDER BY ' + reactionsTableMap[sortCol] + ' ' + order + 
									' SKIP {offset} LIMIT {number}',
					{
						json: reactions,
						searchText: searchText,
						offset: offset,
						number: number,
					}, function(err, result){
						if (err) return console.log(err);
						var final_result = {
										iTotalRecords: parseInt(num_reactions),
										iTotalDisplayRecords: parseInt(num_reactions_filtered),
										sEcho: echo,
										aaData: result.data
									}
						res.send(final_result);	
					}
				);				
			});
		})
	}
	/*var reactionsTableMap = {
        0: "data.entry",
        1: "data.model",
        2: "data.entry",
        3: "data.ecnumber",
        4: "data.geneRule",
        5: "data.name",
        6: "data.entry"
    }
    var offset = parseInt(req.query.iDisplayStart);
    var number = parseInt(req.query.iDisplayLength);
    var sortCol = parseInt(req.query.iSortCol_0);
    var order = req.query.sSortDir_0;
    var echo = parseInt(req.query.sEcho);
    var searchModel = req.query.sSearch_1;
    var models = '["iMM904"]';
    var num_reactions = json.length;
	
	if (searchModel !== ""){
		if (searchModel == "All"){
			models = '["iMM904","iIT341","iJO1366","ymn6_06", ' +
					  '"ymn6_06_cobra","Ec_core","iTO977", ' +
					  '"HMRdatabase_cobra","MODEL1109130000"]'
		}
		else models = '["' + searchModel + '"]';
	}
	
    
	db.cypherQuery(
		'UNWIND {json} AS data ' +
		'WITH data ' + 
		//'UNWIND data.reactants as reacts ' + 
		//'WITH data, reacts ' +
		//'WHERE (data.model IN ' + models + ' ) AND (data.entry =~ {searchText} OR data.name =~ {searchText} OR ' +
		//'      (data.ecnumber IS NOT NULL AND data.ecnumber =~ {searchText}) OR data.geneRule =~ {searchText} OR ' + 
		//'      (data.normalized_rule IS NOT NULL AND data.normalized_rule =~ {searchText})) ' +
		//'WITH data ' + 	
		//'UNWIND data.products as prods ' +
		//'WITH data, prods ' +
		'RETURN {entry: data.entry, reactants: data.reactants, ' + 
		'		 products: data.products, orientation: data.orientation, ' +
		'		 name: data.name, ecnumber: data.ecnumber, gpr_rule: data.gpr_rule, ' +
		'		 gpr_normalized: data.gpr_normalized} ' + 
		'ORDER BY ' + reactionsTableMap[sortCol] + ' ' + order + 
						' SKIP {offset} LIMIT {number}',
		{
			json: json,
			searchText: searchText,
			offset: offset,
			number: number,
			models: models
		//	'UNWIND {json} as data RETURN count(*) ',
			//	{
				//	json: json
		}, function(err, result){
			if (err) console.log(err);
console.log("cheguei");
console.log(result.data);
			//var num_reactions_filtered = result.data.length;
			db.cypherQuery(
				'UNWIND {result} as data RETURN {entry: data.entry, reactants: data.reactants, ' + 
				'	products: data.products, orientation: data.orientation, ' +
				' 	name: data.name, ecnumber: data.ecnumber, gpr_rule: data.gpr_rule, ' +
				'	gpr_normalized: data.gpr_normalized} ' + 
				
				{
					offset: offset,
					number: number,
					result: result.data
				}, function(err, result2){
					//console.log(result.data);
					var final_result = {
							iTotalRecords: parseInt(num_reactions),
							iTotalDisplayRecords: parseInt(num_reactions),
							sEcho: echo,
							aaData: result.data
						}
					res.send(final_result);						
				//}
			//);
		}
	);*/
}

getNeo4jMetabolites = function(req, res, redisClient){
	var speciesTableMap = {
        0: "d.entry",
        1: "d.entry",
        2: "d.name",
        3: "d.compartment",
        4: "d.entry"
    }
    var offset = parseInt(req.query.iDisplayStart);
    var number = parseInt(req.query.iDisplayLength);
    var searchText = req.query.sSearch;
    var sortCol = parseInt(req.query.iSortCol_0);
    var order = req.query.sSortDir_0;
    var echo = parseInt(req.query.sEcho);
    var searchModel = model_map[req.query.sSearch_2];
    var models = '["iMM904"]';
    if (searchModel !== ""){
		if (searchModel == "All"){
			models = model_list
		}
		else models = '["' + searchModel + '"]';
	}
	if (searchText == '') searchText = '.*';
	else {
		if (validateRegex(searchText))
			searchText = '(?i).*' + searchText + '.*';
		else searchText = '.*';
	}
	redisClient.get("all-metabolites", function(err, arr){
		metabolites = JSON.parse(arr);
		db.cypherQuery(
				'MATCH (m:MetaboliteSpecie)-[:has_specie]-(mm:MetabolicModel) ' +
				'WHERE mm.entry IN ' + models + ' ' + 
				'RETURN count(*) as cnt '
			, {
				
			}, function(err, result){
				if (err){
					return console.log(err);
				}
				var num_metabolites = result.data[0];
				db.cypherQuery(
					'UNWIND {json} AS data ' +
					'WITH data ' + 
					'WHERE (data.model IN ' + models + ' ) AND (data.name =~ {searchText} OR data.entry =~ {searchText} ' +
					'      OR data.compartment =~ {searchText}) ' + 
					'WITH count(*) as cnt, collect(data) as dataColl ' +
					'UNWIND dataColl as d ' +
					'RETURN {count: cnt, entry: d.entry, alias: d.alias, model: d.model, name: d.name, compartment: d.compartment} ' +
					'ORDER BY ' + speciesTableMap[sortCol] + ' ' + order + 
									' SKIP {offset} LIMIT {number}',
					{
							json: metabolites,
						searchText: searchText,
						offset: offset,
						number: number,
					}, function(err, result2){
						if (err) return console.log(err);
						var num_metabolites_filtered = 0;
						if (result2.data.length > 0) num_metabolites_filtered = result2.data[0].count;
						var final_result = {
										iTotalRecords: parseInt(num_metabolites),
										iTotalDisplayRecords: parseInt(num_metabolites_filtered),
										sEcho: echo,
										aaData: result2.data
									}
						res.send(final_result);	
					}
				);
			}
		);
	});
}           


exports.findAllReactions = function(req, res){
    var reactionsTableMap = {
        0: "rx.entry",
        1: "rx.entry",
        2: "rx.entry",
        3: "ec.entry",
        4: "rx.geneRule",
        5: "rx.name",
        6: "rx.entry"
    }
    var offset = parseInt(req.query.iDisplayStart);
    var number = parseInt(req.query.iDisplayLength);
    var searchText = req.query.sSearch;
    var sortCol = parseInt(req.query.iSortCol_0);
    var order = req.query.sSortDir_0;
    var echo = parseInt(req.query.sEcho);
    var searchModel = req.query.sSearch_1;
    var model = "iMM904";
    if (searchModel !== "") model = searchModel;
    if (validateRegex(searchText))
        searchText = '(?i).*' + searchText + '.*';
    else searchText = '.*';
    db.cypherQuery(
            
               'MATCH (rx:ModelReaction)-[:has_reaction]-(mod:MetabolicModel {entry: {model}}) ' + 
               'RETURN COUNT(*) as cnt'
            , {
                model: model
            }, function(err, result){
                if (err){
                    return console.log(err);
                }
                var num_reactions = result.data[0];
                db.cypherQuery(
                    'MATCH (rx:ModelReaction)-[:has_reaction]-(mod:MetabolicModel {entry: {model}}), ' +
                     '(rx)-[l:left_component]->(lc:MetaboliteSpecie), ' +
                     '(rx)-[r:right_component]->(rc:MetaboliteSpecie) ' +
                    'OPTIONAL MATCH (rx)-[:has_gpr]-(gpr:Root) ' + 
                    'OPTIONAL MATCH (rx)-[:has_crossreference_to]-(cr)-[:has_ec_number]-(ec) ' +
                    'WITH rx,r,cr,ec,gpr, ' +
                     'COLLECT(DISTINCT {specie: l.cpdEntry, stoichiometry: l.stoichiometry}) as reacts ' +
                    'UNWIND reacts as rcts ' +
                    'WITH rx,r,cr,ec,gpr, rcts, reacts ' +
                    'WHERE rcts.specie =~ {searchText} OR rx.entry =~ {searchText} OR ' +
                     'rx.name =~ {searchText} OR (ec.entry IS NOT NULL AND ec.entry =~ {searchText}) OR rx.geneRule =~ {searchText} ' +
                    'RETURN {entry: rx.entry, ' +
                     'reactants: reacts, ' +
                     'products:COLLECT(DISTINCT {specie: r.cpdEntry, stoichiometry: r.stoichiometry}), ' +
                     'orientation: rx.orientation, name: rx.name, ecnumber: COLLECT(DISTINCT ec.entry), ' +
                     'gpr_rule: rx.geneRule, gpr_normalized: gpr.normalized_rule} ' +
                    'ORDER BY ' + reactionsTableMap[sortCol] + ' ' + order + ' SKIP {offset} LIMIT {number}'
                    , {
                        searchText: searchText,
                        offset: offset,
                        number: number,
                        model: model
                    }, function(err, result2){
                        if (err){
                            return console.log(err);
                        }
                        var final_result = {
                            iTotalRecords: parseInt(num_reactions),
                            iTotalDisplayRecords: parseInt(num_reactions),
                            sEcho: echo,
                            aaData: result2.data
                        }
                        res.send(final_result);
                    }
                );
            }
    );
};

exports.findReactionByEntry = function(req, res){
    var model = req.params.model;
    var reaction = req.params.reaction;
    var entryReaction = reaction + '@' + model;
    db.cypherQuery(
            'MATCH (rx:ModelReaction {entry : {entry}}), ' +
            '   (rx)-[rl:left_component]-(l:MetaboliteSpecie), ' +
            '    (rx)-[rr:right_component]-(r:MetaboliteSpecie) ' +
			'WITH rx, r, rr, COLLECT(DISTINCT {compartment: l.scmp, model_compartment: l.mcpm, alias: l.alias, specie: rl.cpdEntry, ' +
            '                          stoichiometry: rl.stoichiometry}) as reacts ' +
			'WITH rx, reacts, COLLECT(DISTINCT {compartment: r.scmp, model_compartment: r.mcmp, alias: r.alias,specie: rr.cpdEntry, ' +
            '                         stoichiometry: rr.stoichiometry}) as prods ' +
            'OPTIONAL MATCH (rx)-[:has_crossreference_to]-(crossref:Reaction) ' +
            'OPTIONAL MATCH (crossref)-[:has_ec_number]-(ec) ' +
				'OPTIONAL MATCH (rx)-[:has_integrated_reaction]-(iReaction:IntegratedModelReaction)-[:has_integrated_reaction]-(rx2:ModelReaction), ' +
				'               (rx2)-[rl2:left_component]-(l2:MetaboliteSpecie), ' +
				'               (rx2)-[rr2:right_component]-(r2:MetaboliteSpecie), ' +
				'				(rx2)-[:has_reaction]-(mod:MetabolicModel) ' + 
			'WHERE mod.entry IN ' + model_list + ' ' +
            'OPTIONAL MATCH (rx)-[:has_gpr]-(gpr:Root), ' +
            '               (gpr)-[*1..3]->(leaf:Leaf) ' + 
            'OPTIONAL MATCH (leaf)--(gene:SGD) ' + 
            'OPTIONAL MATCH (rx2)-[:has_gpr]-(gpr2:Root) ' +
            'WITH COLLECT(DISTINCT {compartment: l2.scmp, model_compartment: l2.mcmp, alias: l2.alias, specie: rl2.cpdEntry, ' + 
                '             stoichiometry: rl2.stoichiometry}) as model_reacts, ' +
            '     COLLECT(DISTINCT {compartment: r2.scmp, model_compartment: r2.mcmp, alias: r2.alias, specie: rr2.cpdEntry, ' +
                '             stoichiometry: rr2.stoichiometry}) as model_prods, ' +
            '     rx,ec, reacts, prods, crossref, rx2, gpr, gpr2, leaf, gene ' +
            'RETURN {entry:rx.entry, pathway: rx.pathway, model_pathway: rx.mpathway, name: rx.name, ecnumber: COLLECT(DISTINCT ec.entry), ' +
                '       gpr_rule: rx.geneRule, gpr_normalized: gpr.normalized_rule, ' +
        '       reactants: reacts, ' +
        '        products: prods, ' +
        '           orientation: rx.orientation, ' +
        '        crossrefs: COLLECT(DISTINCT {identifier: crossref.entry, ' +
            '                          ref_id: id(crossref), ' +
            '                           database: crossref.major_label}), ' +
        '        modelrefs: COLLECT(DISTINCT {entry: rx2.entry, name: rx2.name, ' +
            '                          orientation: rx2.orientation, ' +
            '                          reactants: model_reacts, ' +
            '                          products: model_prods, ' +
            '                          gpr_rule: rx2.geneRule, ' +
            '                          gpr_normalized: gpr2.normalized_rule }), ' +
            '    genes: COLLECT(DISTINCT {leaf: leaf.entry, sgdgene: gene.entry}) ' +
        '       }',
            {
                entry: entryReaction
            }, function(err, result){
                if (err){
                    return console.log(err);
                }
                console.log(result.data);
                
                res.send(result.data[0]);
                
            }
    );
};

exports.findEqualMetabolites = function(req, res){
    var model = req.params.model;
    var specie = req.params.specie;
    var entry = specie + '@' + model;
    db.cypherQuery(
        'MATCH (m:MetaboliteSpecie {entry: {spcEntry}}) ' +
		'OPTIONAL MATCH (m)-[:has_integrated_specie]-(iSpecie:IntegratedModelSpecie)-[:has_integrated_specie]-(m2:MetaboliteSpecie) ' +
		'WITH m, COLLECT(DISTINCT m2.entry) as integratedSpecies ' +    
        'RETURN {entry: m.entry, ' +
            '    equalSpecies: integratedSpecies } '
        ,{
            spcEntry: entry
        }, function(err, result){
			if (err) return console.log(err);
            var mainModelRefs = result.data[0].equalSpecies;
            mainModelRefs = uniq(mainModelRefs);
            mainModelRefs = removeNull(mainModelRefs);
			var finalResult = {entry: result.data[0].entry,
							   aliases: mainModelRefs}
			res.send(finalResult); 
			
		}
    );
};

exports.findAllSpecies = function(req, res){
    var speciesTableMap = {
        0: "m.entry",
        1: "mm.entry",
        2: "m.name",
        3: "comp.name",
        4: "m.entry"
    }
    var offset = parseInt(req.query.iDisplayStart);
    var number = parseInt(req.query.iDisplayLength);
    var searchText = req.query.sSearch;
    var sortCol = parseInt(req.query.iSortCol_0);
    var order = req.query.sSortDir_0;
    var echo = parseInt(req.query.sEcho);
    var searchModel = req.query.sSearch_2;
    var models = '["iMM904"]';
    if (searchModel !== ""){
		if (searchModel == "All"){
			models = model_list
		}
		else models = '["' + searchModel + '"]';
	}
    searchText = '(?i).*' + searchText + '.*';
    db.cypherQuery(
            'MATCH (m:MetaboliteSpecie)-[:has_specie]-(mm:MetabolicModel) ' +
			'WHERE mm.entry IN ' + models + ' ' + 
            'RETURN count(*) as cnt '
        , {
            
        }, function(err, result){
            if (err){
                return console.log(err);
            }
            var num_species = result.data[0];
            db.cypherQuery(
            'MATCH (m:MetaboliteSpecie)-[:has_specie]-(mm:MetabolicModel) ' +
            'WHERE (mm.entry IN ' + models + ' ) AND (m.name =~ {searchText} OR m.entry =~ {searchText} ' + 
			'       OR m.scmp =~ {searchText}) ' +
            'RETURN {entry: m.entry, alias: m.alias, model: mm.entry, name: m.name, compartment: m.scmp}' +
            'ORDER BY ' + speciesTableMap[sortCol] + ' ' + order + ' SKIP {offset} LIMIT {number}'
            ,{
                searchText: searchText,
                offset: offset,
                number: number
            }, function(err, result2){
                if (err) return console.log(err);
                var final_result = {                
                    iTotalRecords: parseInt(num_species),
                    iTotalDisplayRecords: parseInt(num_species),
                    sEcho: echo,
                    aaData: result2.data
                }
                res.send(final_result);
            }
            );
        }
    );
};

/*
exports.findAllBiggs = function(req, res){
	db.cypherQuery(
		'MATCH (m:MetaboliteSpecie)-[:has_specie]-(mod:MetabolicModel) ' +
		'WHERE mod.entry IN ' + model_list + ' ' + 
		'RETURN DISTINCT m.entry ORDER BY m.entry ASC',
		{
			
		}, function(err, result){
			if (err) return console.log(err);
			
			for (var i = 13000; i < result.data.length; i++){
								console.log(result.data[i]);
				findBigg(result.data[i]);
			}
console.log(result.data.length);
			res.send("OK");
		}
	);
}
	
	
findBigg = function(entrySpecie){
	db.cypherQuery(
        'MATCH (m:MetaboliteSpecie {entry: {entry}}) ' +
        'OPTIONAL MATCH (m)-[:has_crossreference_to]-(cr1:Metabolite) ' + 
        'OPTIONAL MATCH (m)-[:has_specie]-(mm:ModelMetabolite) ' +
        'OPTIONAL MATCH (mm)-[:has_crossreference_to]-(cr2:Metabolite) ' +
        'RETURN {entry: m.entry, ' +
        '        refs1: COLLECT(DISTINCT {database: cr1.major_label, ' + 
            '                                 ref_id: id(cr1), ' +
            '                                 identifier: cr1.entry}), ' +
        '        refs2: COLLECT(DISTINCT {database: cr2.major_label, ' +
            '                                 ref_id: id(cr2), ' +
            '                                 identifier: cr2.entry}) }'
        , {
            entry: entrySpecie
        }, function(err, result){
			if (err) return console.log(err);
			var refs1 = result.data[0].refs1;
            var refs2 = result.data[0].refs2;
            var refs = refs1.concat(refs2).map(function(a){ return a.ref_id; });
            refs = uniq(refs);
            refs = removeNull(refs);
			var finalCrossrefs = null;
			if (refs.length > 0){
				var whereRefs = 'WHERE mm.reference_id IN [' + refs[0];
				for (var i = 1; i < refs.length; i++){
					whereRefs = whereRefs + ',' + refs[i];
				}
				whereRefs = whereRefs + '] ';
				dbMeta.cypherQuery(
					'MATCH (mm:MetaboliteMember) ' +
					whereRefs + ' ' +
					'MATCH (mm)-[:Integrates]-(mc:MetaboliteCluster), ' +
					'      (mc)-[:IntegratedMetaboliteCluster]-(is:IntegrationSet {entry: "FINAL_CORRECT_F"}), ' +
					'      (mc)-[:Integrates]-(mm2:MetaboliteMember) ' +
					'RETURN {ref_id: mm2.reference_id, database: mm2.major_label, identifier: mm2.entry}',
					{
						
					}, function(err, resultMeta){
						if (err) return console.log(err);
						var newCrossrefs = resultMeta.data;
						if (newCrossrefs.length > 0){
							finalCrossrefs = newCrossrefs.concat(refs1, refs2);
						} else {
							finalCrossrefs = refs1.concat(refs2);
						}
						writeBiggToFile(entrySpecie, finalCrossrefs);
					}
				);
			} else {
				console.log("nao tinha bigg");
			}
		}
	);
}

writeBiggToFile = function(entry, crossrefs){
	for (var i = 0; i < crossrefs.length; i++){
		if (crossrefs[i].database === "BiGG"){
			fs.appendFile('reactionsmap.csv', entry + ',' + crossrefs[i].identifier + '\n');
			return 1;
		}
	}
}*/

exports.findSpecieByEntry = function(req, res){
    var model = req.params.model;
    var specie = req.params.specie;
    var entrySpecie = specie + '@' + model;

    db.cypherQuery(
        'MATCH (m:MetaboliteSpecie {entry: {entry}}) ' +
        'OPTIONAL MATCH (m)-[:has_crossreference_to]-(cr1:Metabolite) ' +
        'OPTIONAL MATCH (m)-[:has_integrated_specie]-(iSpecie:IntegratedModelSpecie), ' +
        '               (iSpecie)-[:has_integrated_specie]-(m2:MetaboliteSpecie), ' +
		'				(m2)-[:has_specie]-(mod1:MetabolicModel) ' +
		'WHERE mod1.entry IN ' + model_list  + ' ' +
		'WITH m, m2, cr1 ' +
        'RETURN {entry: m.entry, name: m.name, formula: m.formula, alias: m.alias, compartment: m.scmp, model_compartment: m.mcmp, ' +
		'		 integratedSpecies: COLLECT(DISTINCT {entry: m2.entry, name: m2.name, compartment: m2.scmp}), ' +
        '        refs: COLLECT(DISTINCT {database: cr1.major_label, ' + 
            '                                 ref_id: id(cr1), ' +
		'                                 identifier: cr1.entry}) } '
        , {
            entry: entrySpecie
        }, function(err, result){
            if (err) return console.log(err);
            var newCrossrefs = [];
            var refs = result.data[0].refs;
            db.cypherQuery(
                'MATCH (m:MetaboliteSpecie {entry: {entry}}), ' +
                '      (m)-[:in_compartment]-(comp:SubcellularCompartment) ' +
                'OPTIONAL MATCH (m)-[:right_component]-(rc:ModelReaction) ' +
                'OPTIONAL MATCH (m)-[:left_component]-(lc:ModelReaction) ' +
                'RETURN {reactantrefs: COLLECT(DISTINCT lc.entry), ' +
                    '    productrefs: COLLECT(DISTINCT rc.entry) ' +
                '       }'
				, {
					entry: entrySpecie
				}, function(err, resultReactions){
					reactWhereRefs = "";
					var reactionrefs = resultReactions.data[0].reactantrefs.concat(resultReactions.data[0].productrefs);
					if (reactionrefs.length > 0){
						reactWhereRefs = 'WHERE rx.entry IN ["' + reactionrefs[0];
						for (var i = 1; i < reactionrefs.length; i++){
							reactWhereRefs = reactWhereRefs + '","' + reactionrefs[i];
						}
						reactWhereRefs = reactWhereRefs + '"] ';
					} else {
						reactWhereRefs = "WHERE rx.entry IN [] ";
					} 
					db.cypherQuery(
						'MATCH (rx:ModelReaction), ' +
						'      (rx)-[rl:left_component]-(lm:MetaboliteSpecie), ' +
						'      (rx)-[rr:right_component]-(rm:MetaboliteSpecie) ' +
                        reactWhereRefs + 
                        ' OPTIONAL MATCH (rx)-[has_gpr]-(gpr:Root) ' +
                        'OPTIONAL MATCH (rx)-[:has_crossreference_to]-(crossref:Reaction)-[:has_ec_number]-(ec:EnzymeCommission) ' +
						'RETURN { entry: rx.entry, name: rx.name, orientation: rx.orientation, ' +
                        '         gpr_rule: rx.geneRule, gpr_normalized: gpr.normalized_rule, ' +
                        '         ecnumber: COLLECT (DISTINCT ec.entry), ' +
						'         reactants: COLLECT(DISTINCT {compartment: lm.scmp, alias: lm.alias, specie: rl.cpdEntry, stoichiometry: rl.stoichiometry}), ' +
						'         products: COLLECT(DISTINCT {compartment: rm.scmp, alias: rm.alias, specie: rr.cpdEntry, stoichiometry: rr.stoichiometry}) ' +
						'       }',
						{
						}, function(err, resultReactionRefs){
							if (err) return console.log(err);
							reactionrefs = resultReactionRefs.data;
							var finalModelRefs = result.data[0].integratedSpecies;
							var finalCrossRefs = refs;
							var finalResult = {entry: result.data[0].entry, name: result.data[0].name,
											   alias: result.data[0].alias, 
											   formula: result.data[0].formula,
											   compartment: result.data[0].compartment,
											   model_compartment: result.data[0].model_compartment,
											   reactionrefs: reactionrefs,
											   modelrefs: finalModelRefs,
											   crossrefs: finalCrossRefs
											  }
							res.send(finalResult);
						}  
					);
				}
			);
        }
    );
};

exports.findMetaboliteByEntry = function(req, res){
};

exports.findAllMetabolites = function(req, res){

};

exports.findGPRByReaction = function(req, res){
    var model = req.params.model;
    var reaction = req.params.reaction;
    var entryReaction = reaction + '@' + model;
    db.cypherQuery(
        'MATCH (r:ModelReaction {entry: {entry}}) ' +
        'OPTIONAL MATCH (r)-[:has_gpr]-(gpr:Root), ' +
        '               (gpr)-[*1..3]->(l:Leaf) ' +
        'OPTIONAL MATCH (l)--(s:SGD) ' +
        'RETURN {reaction: r.entry, rule: r.geneRule, normalized_rule: gpr.normalized_rule, ' +
        '        genes: collect(DISTINCT {leaf: l.entry, sgdgene: s.entry}) } '
        ,
        {
            entry: entryReaction
        }, function(err, result){
            if (err) return console.log(err);
			if (result.data.length < 1) return res.send("NOT FOUND");
			
			db.cypherQuery(
				'MATCH (rx:ModelReaction)-[has_gpr]-(gpr:Root) ' +
				'RETURN {reaction: rx.entry, gpr: gpr.normalized_rule} '
				,{
				}, function(err, result2){
					if (err) return console.log(err);
					var gprs = calculateCodesOfGPR(result2.data);
					var score = getSumASCIIfromString(result.data[0].normalized_rule);
					var same_gprs = [];
					var j = 0;
					gprs.sort(dynamicSort("score"));
					for (var i = 0; i < gprs.length; i++){
						if (gprs[i].score > score){
							break;
						}
						if (gprs[i].score == score){
							same_gprs.push(gprs[i]);
						}
					}
					var final_result = {
						reaction: result.data[0].reaction,
						rule: result.data[0].rule,
						normalized_rule: result.data[0].normalized_rule,
						genes: result.data[0].genes,
						same_gprs: same_gprs
					}
					res.send(final_result);
				}
			
			);
        }
    );
};

function dynamicSort(property) {
    var sortOrder = 1;
    if(property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }
    return function (a,b) {
        var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        return result * sortOrder;
    }
}

calculateCodesOfGPR = function(gprs){
	for (var i = 0; i < gprs.length; i++){
		gprs[i].score = getSumASCIIfromString(gprs[i].gpr);
	}
	return gprs;
}

getSumASCIIfromString = function(str){
	var sum_res = 0;
	for (var i = 0; i < str.length; i++){
		sum_res = sum_res + str.charCodeAt(i);
	}
	return sum_res;
}

uniq = function(a) {
    var seen = {};
    return a.filter(function(item) {
        return seen.hasOwnProperty(item) ? false : (seen[item] = true);
    });
}



removeNull = function(a) {
    var result = [];
    for (var i = 0; i < a.length; i++){
        if (a[i] !== null){
            result.push(a[i]);
        }
    }
    return result;
}

function validateRegex(pattern) {
    var parts = pattern.split('/'),
    regex = pattern,
    options = "";
    if (parts.length > 1) {
	regex = parts[1];
	options = parts[2];
    }
    try {
	new RegExp(regex, options);
	return true;
    }
    catch(e) {
	return false;
    }
}
