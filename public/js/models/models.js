window.SGDGene = Backbone.Model.extend({
    urlRoot: 'sgdgenes',

    idAttribute: 'entry'
    
});

window.SGDGeneCollection = Backbone.Collection.extend({
    model : SGDGene,

    url: 'sgdgenes'
});


window.GPR = Backbone.Model.extend({
    urlRoot: 'gprs',

    initialize: function(options){
        if (options !== null)
            this.url = 'gprs/'+options.model + '/' + options.reaction;
    }
});

window.GPRCollection = Backbone.Collection.extend({
    model: GPR, 
    url: 'gprs'
});


window.Reaction = Backbone.Model.extend({
    urlRoot: 'reactions',
    initialize: function(options){
        if (options !== null) 
            this.url = 'reactions/'+options.model + '/' + options.reaction;
    }
});

window.ReactionCollection = Backbone.Collection.extend({
    model: Reaction,
    url: 'reactions'
});


window.Specie = Backbone.Model.extend({
    urlRoot: 'species',
    initialize: function(options){
        if (options !== null)
            this.url= 'species/'+options.model + '/' + options.specie;
    }
});

window.SpecieCollection = Backbone.Collection.extend({
    model: Specie,
    url: 'species'
});


window.Metabolite = Backbone.Model.extend({
    urlRoot: 'metabolites',
    initialize: function(options){
        if (options !== null)
            this.url = 'metabolites/' + options.model + '/' + options.metabolite;
    }
});

window.MetaboliteCollection = Backbone.Collection.extend({
    model: Metabolite,
    url: 'metabolites'
});

window.Home = Backbone.Collection.extend({
    urlRoot: '/'
});
