var compartments = {"Golgi Membrane": "gm",
                     "Vacuolar Membrane": "vm",
                     "Endoplasmic Reticulum Membrane": "rm",
                     "Peroxisomal Membrane": "xm",
                     "Cytoplasmic Membrane": "cm",
                     "Plasma Membrane" : "cm",
                     "Cytosol": "c",
                     "Golgi Apparatus": "g",
                     "Extracellular": "e",
                     "Extra Cellular" : "e",
                     "Nucleus": "n",
                     "Mitochondrion" : "m",
                     "Mitochondria": "m",
                     "Vacuole": "v",
                     "Endoplasmic Reticulum": "r",
                     "Mitochondrial Membrane": "mm",
                     "Peroxisome": "x",
                     "Nuclear Membrane": "nm",
                     "Boundary": "b",
                     "Lipid Particle": "lp",
                     "Cell Envelope": "ce"};


var model_list = ["iIN800", 
       "Yeast 6","iTO977",
       "iFF708", 
       "Yeast 1","iAZ900","iND750","iLL672", 
       "Yeast 7", "All"] //o iMM904 ja esta no template


var model_map = {
    "iIN800" : "iIN800",
    "ymn6_06_cobra" : "Yeast 6",
    "iTO977" : "iTO977",
    "iFF708" : "iFF708",
    "ymn1_0" : "Yeast 1",
    "iAZ900" : "iAZ900",
    "MODELID_222668" : "iND750",
    "iLL672" : "iLL672",
    "ymn_7_6_cobra" : "Yeast 7",
    "iMM904" : "iMM904"
}



window.SGDGeneView = Backbone.View.extend({
    
    template: _.template($('#sgdgene-template').html()),

    initialize: function(){
        this.render();
    },

    render: function(){
        $(this.el).html(this.template(this.model.toJSON()));
    }
});


window.SGDGeneListView = Backbone.View.extend({

    template: _.template($('#sgdgenelist-template').html()),    

    initialize: function(){
        this.render();
    },

    render: function(){
        //var genes = this.model.models;
        //var len = genes.length;
        //var startPos = 0;
        //var endPos = len;

        $(this.el).html(this.template);
        //for (var i = startPos; i < endPos; i++){
        //    $('.sgdgene-list', this.el).append(new SGDGeneListItemView({model: genes[i]}).render().el);
        //}   
        //return this;
    }

});


window.SGDGeneListItemView = Backbone.View.extend({
    tagName: 'tr',

    template: _.template($('#sgdgenelistitem-template').html()),

    initialize: function(){
        this.model.bind("change", this.render, this);
        this.model.bind("destroy", this.close, this);
    },

    render:function(){
        $(this.el).html(this.template(this.model.toJSON()));
        return this;
    }

});


window.ReactionView = Backbone.View.extend({
    template: _.template($('#reaction-template').html()),

    initialize: function(){
        this.render();
    },

    render: function(){
        $(this.el).html(this.template(this.model.toJSON()));
    }
});

window.ReactionListView = Backbone.View.extend({
    template: _.template($('#reactionlist-template').html()),

    initialize: function(){
        this.render();
    },

    render: function(){
        //var reactions = this.model.models;
        //var len = reactions.length;
        $(this.el).html(this.template);
        /*for (var i = 0; i < len; i++){
            $('.reaction-list', this.el).append(new ReactionListItemView({model: reactions[i]}).render().el);
        }

        return this;*/
    }
});

window.ReactionListItemView = Backbone.View.extend({
    tagName: 'tr',
    
    template: _.template($('#reactionlistitem-template').html()),

    initialize: function(){
        this.model.bind("change", this.render, this);
        this.model.bind("destroy", this.close, this);
    },

    render: function(){
        $(this.el).html(this.template(this.model.toJSON()));
        return this;
    }
});


window.SpecieView = Backbone.View.extend({
    template: _.template($('#specie-template').html()),

    initialize: function(){
        this.render();
    },

    render: function(){
        $(this.el).html(this.template(this.model.toJSON()));
        return this;
    }
});

window.SpecieListView = Backbone.View.extend({
    template: _.template($('#specielist-template').html()),

    initialize: function(){
        this.render();
    },

    render: function(){
       // var species = this.model.models;
       // var len = species.length;
        $(this.el).html(this.template);
       /* for (var i = 0; i < len; i++){
            $('.specie-list', this.el).append(new SpecieListItemView({ model: species[i]}).render().el);
        }
        return this;*/
    }

});

window.SpecieListItemView = Backbone.View.extend({
    tagName: 'tr',

    template: _.template($('#specielistitem-template').html()),

    initialize: function(){
       this.model.bind("change", this.render, this);
       this.model.bind("destroy", this.close, this);
    },

    render: function(){
        $(this.el).html(this.template(this.model.toJSON()));
        return this;    
    }
});


window.MetaboliteView = Backbone.View.extend({
    template: _.template($('#metabolite-template').html()),

    initialize: function(){
        this.render();
    },

    render: function(){
        $(this.el).html(this.template(this.model.toJSON()));
        return this;
    }
});

window.MetaboliteListView = Backbone.View.extend({
    template: _.template($('#metabolitelist-template').html()),

    initialize: function(){
        this.render();
    },

    render: function(){
        var metabolites = this.model.models;
        var len = metabolites.length;
        $(this.el).html(this.template);

        for (var i = 0; i < len; i++){
            $('.metabolite-list', this.el).append(new MetaboliteListItemView({ model: metabolites[i]}).render().el);
        }
    }
});

window.MetaboliteListItemView = Backbone.View.extend({
    tagName: 'tr',

    template: _.template($('#metabolitelistitem-template').html()),

    initialize: function(){
        this.model.bind("change", this.render, this);
        this.model.bind("destroy", this.close, this);
    },

    render: function(){
        $(this.el).html(this.template(this.model.toJSON()));
        return this;
    }
});


window.GPRView = Backbone.View.extend({
    template: _.template($('#gpr-template').html()),

    initialize: function(){
        this.render();
    },

    render: function(){
        $(this.el).html(this.template(this.model.toJSON()));
        return this;
    }
});

window.HomeView = Backbone.View.extend({
    template: _.template($('#home-template').html()),

    initialize: function(){
        this.render();
    },

    render: function(){
        $(this.el).html(this.template(this.model.toJSON()));
        return this;
    }
});

window.CompartmentsView = Backbone.View.extend({
    template: _.template($('#compartments-template').html()),

    initialize: function(){
        this.render();
    },

    render: function(){
        $(this.el).html(this.template());
        return this;
    }
});

window.AboutView = Backbone.View.extend({
    template: _.template($('#about-template').html()),

    initialize: function(){
        this.render();
    },

    render: function(){
        $(this.el).html(this.template());
        return this;
    }
});

var AppRouter = Backbone.Router.extend({
    
    routes : {
        ""                                  : "home",
        "sgdgenes"                          : "geneList",
        "sgdgenes/:entry"                   : "geneDetails",
        "reactions"                         : "reactionList",
        "reactions/:model/:reaction"        : "reactionDetails",
        "species"                           : "specieList",
        "species/:model/:specie"            : "specieDetails",
        "metabolites"                       : "metaboliteList",
        "metabolites/:model/:metabolite"    : "metaboliteDetails",
        "gprs/:model/:reaction"             : "gprDetails",
        "compartments"                      : "compartmentsDetails",
        "biomass"                           : "biomassDetails",
        "about"                             : "aboutDetails"
    },

    initialize: function(){
        //this.headerView = new HeaderView();
        //$('.header').html(this.headerView.el);
    },  

    compartmentsDetails: function(){
        $('#main-content').html(new CompartmentsView().el);
    },

    biomassDetails: function(){
        $('#main-content').html(new BiomassView().el);
    },

    aboutDetails: function(){
        $('#main-content').html(new AboutView().el);
    },

    home: function(){
        /*if (!this.homeView){
            this.homeView = new HomeView();
        }
        $('#main-content').html(this.homeView.el);*/
    },

    geneList: function(){
        //var sgdgeneList = new SGDGeneCollection();
        //sgdgeneList.fetch({success: function(){
        //    $('#main-content').html(new SGDGeneListView({ model: sgdgeneList}).el);
        //}});
        $('#main-content').html(new SGDGeneListView().el);
        initializeGeneTable('#sgdgene-table');
        //initializeTable("#sgdgene-table");
    },

    geneDetails: function(entry){
        //if (this.sgdgeneList === null) this.geneList();
        var gene = new SGDGene({entry: entry});
        gene.fetch({success: function(){
            $('#main-content').html(new SGDGeneView({model: gene}).el);  
            initializeGeneReactionsRefs("#sgd-reactionrefs-table");
        }});
    },

    reactionList: function(){
        /*var reactionlist = new ReactionCollection();
        reactionlist.fetch({success: function(){
            $('#main-content').html(new ReactionListView({ model: reactionlist}).el);
        }});*/
        $('#main-content').html(new ReactionListView().el);
        initializeReactionTable("#reaction-table");
        //initializeTable('#reaction-table');
    },
    
    reactionDetails: function(model, reaction){
        //if (this.reactionlist === null) this.reactionList();
        var rx = new Reaction({model: model, reaction : reaction});
        rx.fetch({success: function(){
            $('#main-content').html(new ReactionView({model : rx}).el);
            var metabolites = rx.attributes;
            if ( metabolites.modelrefs.length == 1 && metabolites.modelrefs[0].entry == null){
                initializeReactionModelRefsTable('#modelrefs-table', null, null);
            } else {
                initializeReactionModelRefsTable('#modelrefs-table', model, metabolites);
            }
        }});
    },

    specieList: function(){
        /*var specielist = new SpecieCollection();
        specielist.fetch({success: function(){
            $('#main-content').html(new SpecieListView({ model: specielist}).el);
        }});*/
        $('#main-content').html(new SpecieListView().el);
        initializeSpeciesTable('#specie-table');
        //initializeTable('#specie-table');
    },

    specieDetails: function(model, specie){
        //if (this.specielist === null) this.specieList();
        var spc = new Specie({model: model, specie: specie});
        spc.fetch({success: function(){
            $('#main-content').html(new SpecieView({model: spc}).el);
            initializeSpeciesModelRefsTable('#metabolite-modelrefs-table');
            initializeReactionModelRefsTable('#metabolite-reactionrefs-table', null, null);
        }});
    },

    metaboliteList: function(){
        var metabolitelist = new MetaboliteCollection();
        metabolitelist.fetch({ success: function(){
            $('#main-content').html(new MetaboliteListView({ model: metabolitelist}).el);
        }});
        initializeTable('#metabolite-table');
    },

    metaboliteDetails: function(model, metabolite){
        if (this.metabolitelist === null) this.metabolitelist();
        var metab = new Metabolite({model: model, metabolite: metabolite});
        metab.fetch({ success: function(){
            $('#main-content').html(new MetaboliteView({model: metab}).el);
        }});
    },

    gprDetails: function(model, reaction){
        var gpr = new GPR({model: model, reaction: reaction});
        gpr.fetch({ success: function(){
            $('#main-content').html(new GPRView({model: gpr}).el);
            initializeGeneReactionsRefs("#gpr-reactionrefs-table");
        }});
    }

});

getColors = function(n){
    var h = 0;
    var colors = [];
    for (var i = 0; i < n; i++) {
        var color = new KolorWheel([h,72,57]);
        colors.push(color.getHex());
        h += 35;
    }
    return colors;
}

getEqualMetabolites = function(table, model, data){
    var deferreds = [];
    var results = [];
    var metabolites = data.reactants.concat(data.products).map(function(a){ return a.specie; });
    var n = data.reactants.length + data.products.length;
    var colors = getColors(n);

    for (var i = 0; i < data.reactants.length; i++){
        data.reactants[i].color = colors[i];
    }
    for (var i = 0; i < data.products.length; i++){
        data.products[i].color = colors[i+data.reactants.length];
    }

    var newEquation = getEquation(data, true);
    $('#equation').html(newEquation);

    $.each(metabolites, function(index, metabolite){
        deferreds.push(
            $.ajax({
                url: "metabolite/" + model + "/" + metabolite,
                type: "GET",
                success: function(species){
                    results.push(species);
                    return true;
                }
            })
        );
    });
    $.when.apply($, deferreds).then(function(){
        for (var i = 0; i < data.reactants.length; i++){
            for (var j = 0; j < results.length; j++){
                if (data.reactants[i].specie === results[j].entry.split("@")[0]){
                    data.reactants[i].aliases = results[j].aliases;
                }
            }
        }
        for (var i = 0; i < data.products.length; i++){
            for (var j = 0; j < results.length; j++){
                if (data.products[i].specie === results[j].entry.split("@")[0]){
                    data.products[i].aliases = results[j].aliases;
                }
            }
        }
        updateEquationCells(table, data);

    });
}
 
updateEquationCells = function(table, data){
    var nRows = table.fnGetData().length;
    for (var i = 0; i < data.modelrefs.length; i++){
        if (data.modelrefs[i].entry === null)
            data.modelrefs.splice(i,1);
    }
    
    for (var i = 0; i < nRows; i++){
            updateEquationCell(table, data, i);
    }
}

updateEquationCell = function(table, data, index){
    var model = data.modelrefs[index].entry.split("@")[1];
    var newEquation = [];
    newEquation.entry = data.modelrefs[index].entry;
    newEquation.orientation = data.modelrefs[index].orientation;
    newEquation.reactants = getNewReactants(data, model,index);
    newEquation.products = getNewProducts(data, model, index);
    
    var equationHtml = getEquation(newEquation, true);

    table.fnUpdate(equationHtml, index, 2);
    $('td', table.fnGetNodes(index)).eq(2).attr('data-content', equationHtml);
}

getNewProducts = function(data, model, index){
    var count_prods = data.modelrefs[index].products.length;
    var newProducts = []
    var matched_prods = [];

    for (var i = 0; i < data.products.length; i++){
        if (count_prods > 0) {
            for (var j = 0; j < data.modelrefs[index].products.length; j++){
                if (data.products[i].aliases.indexOf(data.modelrefs[index].products[j].specie + '@' + model) >= 0){
                    newProducts.push( { specie: data.modelrefs[index].products[j].specie,
                        stoichiometry: data.modelrefs[index].products[j].stoichiometry,
                        compartment: data.modelrefs[index].products[j].compartment,
                        alias: data.modelrefs[index].products[j].alias,
                        color: data.products[i].color
                    });
                    count_prods--;
                    matched_prods.push(data.modelrefs[index].products[j].specie);
                }
            }
        } else {
            break;
        }
    }
    if (count_prods > 0){
        for (var i = 0; i < data.modelrefs[index].products.length; i++){
            if (matched_prods.indexOf(data.modelrefs[index].products[i].specie) < 0){
                newProducts.push( { specie: data.modelrefs[index].products[i].specie,
                    stoichiometry: data.modelrefs[index].products[i].stoichiometry,
                    compartment: data.modelrefs[index].products[i].compartment,
                    alias: data.modelrefs[index].products[i].alias,
                    color: "#999966"
                });
            }
        }
    }
    return newProducts;
}

getNewReactants = function(data, model, index){
    var count_reacts = data.modelrefs[index].reactants.length;
    var newReactants = []; 
    matched_reacts = [];
    for (var i = 0; i < data.reactants.length; i++){
        if (count_reacts > 0) {
            for (var j = 0; j < data.modelrefs[index].reactants.length; j++){
                if (data.reactants[i].aliases.indexOf(data.modelrefs[index].reactants[j].specie + '@' + model) >= 0){
                    newReactants.push( { specie: data.modelrefs[index].reactants[j].specie,
                                                  stoichiometry: data.modelrefs[index].reactants[j].stoichiometry,
                                                  alias: data.modelrefs[index].reactants[j].alias,
                                                  color: data.reactants[i].color,
                                                  compartment: data.reactants[i].compartment
                                                });
                    count_reacts--;
                    matched_reacts.push(data.modelrefs[index].reactants[j].specie);
                }
            }
        } else {
            break;
        }
    }
    if (count_reacts > 0){
        for (var i = 0; i < data.modelrefs[index].reactants.length; i++){
            if (matched_reacts.indexOf(data.modelrefs[index].reactants[i].specie) < 0){
                newReactants.push( { specie: data.modelrefs[index].reactants[i].specie,
                                    stoichiometry: data.modelrefs[index].reactants[i].stoichiometry,
                                    alias: data.modelrefs[index].reactants[i].alias,
                                    compartment: data.modelrefs[index].reactants[i].compartment,
                                    color: "#999966"
                                  });
            }
        }
    }
    return newReactants;
}

initializeTable = function(tablename){
    $(tablename).waitUntilExists(function(){
        $(tablename).DataTable({
            "responsive" : true   
        });
    });
    /*table.columns().every( function () {
        var that = this;
                       
        $( '.searchInput').on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        });
    });*/
};

initializeGeneReactionsRefs = function(tablename){
    $(tablename).waitUntilExists(function(){
        var table = $(tablename).dataTable({
            "responsive": true,
            "aoColumns": [
                {
                    "sWidth": "15%",
                    "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
                        nTd.title = sData;
                    }
                },
                {
                    "sWidth": "20%",
                    "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
			if (sData != null && sData != '')                        
				nTd.title = sData;
                    }
                },
                {
                    "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
                        $(nTd).attr('data-content', sData);
                        $(nTd).popover({
                            html: true,
                            trigger: 'hover',
                            placement: 'top',
                            container: $(nTd)
                        });
                    }
                },
                {
                    "sWidth": "3%",
                    "orderable": false
                }
            ]
        });
    });
}

initializeReactionModelRefsTable = function(tablename, model, data){
    //var dom_value = 'lrtip';
    //if (model !== null && data !== null){
    var dom_value = "lfrtip<'row'>B";
    //}

    $(tablename).waitUntilExists(function(){
        var table = $(tablename).dataTable({
            "responsive": true,
            "aoColumns": [
                {
                    "sWidth": "9%",
                    "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
                        if (sData != null && sData != '') nTd.title = sData;
                    }
                }, 
                {
                    "sWidth": "7%",
                    "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
                        if (sData != null && sData != '') nTd.title = sData;
                    }
                },
                {
                    "sWidth": "40%",
                    "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
                        $(nTd).attr('data-content', sData);
                        $(nTd).popover({
                            html: true,
                            trigger: 'hover',
                            placement: 'top',
                            container: $(nTd)
                        });
                    }
                },
                {
                    "sWidth": "10%",
                    "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
                        if (sData != null && sData != '') nTd.title = sData;
                    }
                },
                {
                    "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
			if (sData != null && sData != ''){                        
		                $(nTd).attr('data-content', sData);
		                $(nTd).popover({
		                    html: true,
		                    trigger: 'hover',
		                    placement: 'top',
		                    container: $(nTd)
		                });
			}
                    }
                },
                {
                    "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
                        if (sData != null && sData != '') nTd.title = sData;
                    }
                },
                {
                    "sWidth": "3%",
                    "orderable": false
                }
            ],
            dom: dom_value,
                buttons: [
                {
                    extend: 'colvisGroup',
                    text: 'Equation',
                    show: [0,1,2,6],
                    hide: [3,4,5]
                }, 
                {
                    extend: 'colvisGroup',
                    text: 'GPR',
                    show: [0,1,4,6],
                    hide: [2,3,5]
                },
                {
                    extend: 'colvisGroup',
                    text: 'Equation+GPR',
                    show: [0,1,2,4,6],
                    hide: [3,5]
                },
                {
                    extend: 'colvisGroup',
                    text: 'Show all',
                    show: ':hidden'
                }
            ]
        });
        if (model !== null && data !== null) getEqualMetabolites(table, model, data);
    });
};

initializeSpeciesModelRefsTable = function(tablename){
    $(tablename).waitUntilExists(function(){
        $(tablename).dataTable({
            "responsive" : true,
            "aoColumns": [
                {
                    "sWidth": "15%",
                    "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
                        if (sData != null && sData != '') nTd.title = sData;
                    }      
                },
                {
                    "sWidth" : "15%",
                    "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
                        if (sData != null && sData != '') nTd.title = sData;
                    }
                },
                {
                    "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
                        if (sData != null && sData != '') nTd.title = sData;
                    }
                },
                {
                    "sWidth": "15%",
                    "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
                        if (sData != null && sData != '') nTd.title = sData;
                    }
                },
                {
                    "sWidth": "3%",
                    "orderable": false
                }
            ]
        });
    });
};

initializeSpeciesTable = function(tablename){
    $(tablename).waitUntilExists(function(){
        $(tablename).dataTable({
            "responsive"  : true,
            "bProcessing" : true,
            "bServerSide" : true,
            "bStateSave": true,
            "sAjaxSource" : 'species',
            "aoColumns" : [
                {   "sWidth": "15%",
                    "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
                        if (sData != null && sData != '') nTd.title = sData;
                    },
                    mData: function(source, type, val){
                    var spc = source.entry.split("@")[0];
                    return spc;
                 }
                },

                {
                   "sWidth": "15%",
                    "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
                        if (sData != null && sData != '') nTd.title = sData;
                    },
                    mData: function(source, type, val){
                        var alias = source.alias;
                        return alias;
                    }
                },
                {  "sWidth": "10%",
                    "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
                        if (sData != null && sData != '') nTd.title = sData;
                    },
                    mData: function(source, type, val){
                    var model = model_map[source.entry.split("@")[1]];
                    return model;
                 }
                },
                { "fnCreatedCell" : function(nTd, sData, oData, iRow, iCol){
                        if (sData != null && sData != '') nTd.title = sData;
                  },
                  mData: function(source, type, val){
                    return source.name;
                 }
                },
                { "sWidth": "15%",
                  "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
                        if (sData != null && sData != '') nTd.title = sData;
                  },
                  mData: function(source, type, val){
                    return source.compartment;
                 }
                },
                { "sWidth": "3%",
                  "orderable": false,
                  mData: function(source, type, val){
                    var spc = source.entry.split("@")[0];
                    var model = source.entry.split("@")[1];
                    var more = '<a href="#species/'+ model + '/' + spc + '"><span class="fa fa-search"></span></a>';
                    return more;
                 }
                }
               ]
        }).columnFilter({
                        aoColumns : [
                                        null,
                                        null,
                                    {
                                        type: "select",
                                        values : model_list
                                    },
                                    null,
                                    null,
                                    null
                                    ]
        });
    });
}

initializeGeneTable = function(tablename){
    $(tablename).waitUntilExists(function(){
        $(tablename).dataTable({
             "responsive"  : true,
             "bProcessing" : true,
             "bServerSide" : true,
             "bStateSave": true,
             "sAjaxSource" : 'sgdgenes',
             "aoColumns" : [
                { mData: function(source, type, val){
                            return source.entry;
                         }
                },
                { mData: function(source, type, val){
                            if (source.systematic_name === null || 
                                source.systematic_name === 'null') return '';
                            return source.systematic_name;
                         }
                },
                { mData: function(source, type, val){
                            if (source.name === null || 
                                source.name === 'null') return '';
                            return source.name;
                         }
                },
                {   "orderable": false,
                    mData: function(source, type, val){
                            var entry = source.entry;
                            var more = '<a href="#sgdgenes/'+ entry + '"><span class="fa fa-search"></span></a>';
                            return more;
                         }
                }
             ]
        });
    });
}

initializeReactionTable = function(tablename){
    var dom_value = "lfrtip<'row'>B";
    $(tablename).waitUntilExists(function(){
        $(tablename).dataTable({
            "responsive"  : true,
            "bProcessing" : true,
            "bServerSide" : true,
            "bStateSave": true,
            "sAjaxSource" : 'reactions',
            "aoColumns" : [
                {   "sWidth": "9%",
                    "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
                        if (sData != null && sData != '') nTd.title = sData;
                    },
                    mData: function(source, type, val){
                        var rx = source.entry.split("@")[0];
                        return rx;
                    }                                   
                },
                {   "sWidth": "7%",
                    "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
                        if (sData != null && sData != '') nTd.title = sData;
                    },
                    mData:  function(source,type,val){
                        var model = model_map[source.entry.split("@")[1]];                
                        return model;
                  }
                },
                {   "sWidth": "33%",
                    "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
                        //var equationTooltip = sData.replace(/<([a-z]|[\/])[^>]*>/g, ' ');
                        $(nTd).attr('data-content', sData);
                        $(nTd).popover({
                            html: true,
                            viewport: '#reaction-table_wrapper',
                            trigger: 'hover',
                            placement: 'right',
                            container: $(nTd)
                        });
                    },
                    mData:  function(source,type,val){
                        var equation = getEquation(source, false);
                        return equation;                        
                 }
                },
                {  "sWidth": "10%",
                    "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
                        if (sData != null && sData != '') nTd.title = sData;
                    },
                    mData:  function(source,type,val){
                        var ec = source.ecnumber;
                        if (ec === "") return null;
                        return ec;                        
                 }
                },
                {   "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
			if (sData != null && sData != ''){                        
				$(nTd).attr('data-content', sData);
		                $(nTd).popover({
				    html: true,
		                    trigger: 'hover',
		                    placement: 'left',
		                    container: $(nTd)
		                });
			}
                    },
                    mData:  function(source,type,val){
						var model = source.entry.split('@')[1];
						var rx = source.entry.split('@')[0];
                        var gpr_norm = source.gpr_normalized;
                        var gpr = source.gpr_rule;
                        if (gpr_norm !== null){
                            return '<a href="#gprs/' + model + '/' + rx + '">' + gpr_norm + '</a>';
                        } else if (gpr != "")
                        	return '<a href="#gprs/' + model + '/' + rx + '">' + gpr + '</a>'; 
			return null;                       
                 }
                },
                {   "sWitdh": "10%",
                    "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
                        if (sData != null && sData != '') nTd.title = sData;
                    },
                    mData: function(source, type, val){
                        var pathway = source.pathway;
                        return pathway;
                    }
                },
                {   "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
                        if (sData != null && sData != '') nTd.title = sData;
                    },
                    mData:  function(source,type,val){
                        var name = source.name;
                        return name;                        
                 }
                },
                {   "sWidth": "1.5%",
                    "orderable": false,
                    mData:  function(source,type,val){
                        var model = source.entry.split("@")[1];
                        var rx = source.entry.split("@")[0];
                        var more = '<a href="#reactions/'+ model + '/' + rx + '"><span class="fa fa-search"></span></a>';
                        return more;
                 }
                }
            ],
                dom: dom_value,
                buttons: [
                {
                    extend: 'colvisGroup',
                    text: 'Equation',
                    show: [0,1,2,7],
                    hide: [3,4,5,6]
                },
                {
                    extend: 'colvisGroup',
                    text: 'GPR',
                    show: [0,1,4,7],
                    hide: [2,3,5,6]
                },
                {
                    extend: 'colvisGroup',
                    text: 'Equation+GPR',
                    show: [0,1,2,4,7],
                    hide: [3,5,6]
                },
                {
                    extend: 'colvisGroup',
                    text: 'Show all',
                    show: ':hidden'
                }
            ]
        }).columnFilter({
            aoColumns : [
                            null,
                            {
                                type: "select",
                                values: model_list
                            },
                            null,
                            null,
                            null,
                            null,
                            null
                        ]  
        });
    });
}


getEquation = function(data, color){
    var model = data.entry.split("@")[1];
    var equation = '';
    var reactComps = data.reactants.map(function(a){ return a.compartment; });
    var prodComps = data.products.map(function(a){ return a.compartment; });
    var comps = reactComps.concat(prodComps).unique();
    var differentComps = comps.length > 1;
    var comp_color = '#ff9900';
    
    if (!differentComps) equation = '<span title="' + comps[0] + '" class="btn-primary btn-xs" style="font-size: small; background-color: ' + comp_color + ';">' + compartments[comps[0]] + '</span>&nbsp';

        for (var i = 0; i < data.reactants.length; i++){
            var style = '';
            if (color){
                style = 'style="background-color: ' + data.reactants[i].color + ';"';
            }
            var react = '';
            if (data.reactants[i].alias === null) react = data.reactants[i].specie;
            else react = data.reactants[i].alias;
            var stoic = '';
            if (data.reactants[i].stoichiometry != 1){
                stoic = data.reactants[i].stoichiometry;
            }
            equation = equation + '<span class="btn-xs" style="padding: 0;"> ' + stoic + ' </span><a title="' + data.reactants[i].specie + '" class="btn-primary btn-xs" ' + style + ' href="#species/' + model + '/' + data.reactants[i].specie+ '">' + react + '</a>'; 
            
             if (differentComps) equation = equation + '<span title="' + data.reactants[i].compartment + '" class="btn-primary btn-xs" style="background-color: ' + comp_color + ';">' + compartments[data.reactants[i].compartment] + '</span>';
            
            if (i !== data.reactants.length -1){
                equation = equation + ' + ';
            }            
        }
        if (data.orientation == "LeftToRight") {
            equation = equation + ' => ';
        } else if (data.orientation == "Reversible"){
            equation = equation + ' <=> ';
        } else if (data.orientation == "RightToLeft"){
            equation = equation + ' <= ';
        } else {
            equation = equation + ' ? ';
        }
        
        for (var i = 0; i < data.products.length; i++){
            var style = '';
            var prod = '';
            
            if (data.products[i].alias === null) prod = data.products[i].specie;
            else prod = data.products[i].alias;

            if (color){
                style = 'style="background-color: ' + data.products[i].color + ';"';
            } 
            
            var stoic = '';
            if (data.products[i].stoichiometry != 1){
                stoic = data.products[i].stoichiometry;
            }
            equation = equation + '<span class="btn-xs" style="padding: 0;"> ' + stoic + ' </span><a title="' + data.products[i].specie + '" class="btn-primary btn-xs" ' + style + ' href="#species/' + model + '/' + data.products[i].specie + '">' + prod + '</a>';
            
            if (differentComps)  equation = equation + '<span title="' + data.products[i].compartment + '" class="btn-primary btn-xs" style="background-color: ' + comp_color + ';">' + compartments[data.products[i].compartment] + '</span>';
            
            if (i !== data.products.length -1){
                equation = equation + ' + ';
            }
        }
        return equation;
}

getGeneRef = function(database, identifier){
    if (database == "Uniprot"){
        return 'http://www.uniprot.org/uniprot/' + identifier;
    } 
    if (database == 'Expasy'){
        return 'http://enzyme.expasy.org/EC/' + identifier;
    }
}

getMetaboliteRef = function(crossref){
    if (crossref.database == 'LigandCompound'){
        return 'http://www.kegg.jp/dbget-bin/www_bget?cpd:' + crossref.identifier;    
    }
    if (crossref.database == 'LigandGlycan') {
        return 'http://www.kegg.jp/dbget-bin/www_bget?gl:' + crossref.identifier;
    }
    if (crossref.database == 'LigandDrug') {
        return 'http://www.kegg.jp/dbget-bin/www_bget?dr:' + crossref.identifier;
    }
    if (crossref.database == 'ChEBI') {
        return 'http://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI%3A' + crossref.identifier;
    }
    if (crossref.database == 'MetaCyc') {
        var pgdb = crossref.identifier.split(":");
        return 'http://biocyc.org/' + pgdb[0] + '/NEW-IMAGE?type=COMPOUND&object=' + pgdb[1];
    }
    if (crossref.database == 'Seed') {
        return 'http://seed-viewer.theseed.org/seedviewer.cgi?page=CompoundViewer&compound=' + crossref.identifier + '&model=';
    }
    if (crossref.database == 'PubChemSubstance') {
        return 'http://pubchem.ncbi.nlm.nih.gov/substance/' + crossref.identifier + '?from=summary';
    }
    if (crossref.database == 'PubChemCompound') {
        return 'https://pubchem.ncbi.nlm.nih.gov/compound/' + crossref.identifier;
    }
    if (crossref.database == 'BiGG'){
        return 'http://bigg.ucsd.edu/universal/metabolites/' + crossref.identifier;
    }
    if (crossref.database == 'HMDB'){
        return 'http://hmdb.ca/metabolites/' + crossref.identifier;
    }

}

window.BiomassView = Backbone.View.extend({
  template: _.template($('#biomass-template').html()),

  initialize: function(){
    this.render();
  },

  render: function(){
    var modelData = [{"iMM904" : ["R_biomass_SC5_notrace"]},
                     {"iAZ900" : ["R_biomass_wild", "R_biomass_core"]},
                     {"iFF708" : ["R_VGRO"]},
                     {"iLL672" : ["R_1095"]},
                     {"MODELID_222668" : ["R_biomass_SC4_bal"]},
                     {"iTO977" : ["R_NBIOMASS", "R_CBIOMASS"]},
                     {"ymn6_06_cobra" : ["r_2133", "r_2110"]},
                     {"ymn_7_6_cobra" : ["r_4041"]}];
    var speciesData = {
      "h" : [58.70001, 58.70001, 58.70001, 0, 0, 58.7162, 0, 0, 59.4697, 58.7, 59.4697],
      "pi" : [59.305, 59.305, 59.305, 59.305, 23.9456, 59.305, 59.305, 59.305, 58.70001, 59.3, 58.70001],
      "adp" : [59.276, 59.276, 59.276, 59.276, 23.91266, 59.276, 59.276, 59.276, 59.276, 59.3, 59.276],
      "atp" : [-59.276, -62.9616, -62.9616, -59.276, -23.9166, -59.276, -59.276, -59.276, -62.9616, -59.3, -62.9616],
      "so4" : [-0.02, -0.02, -0.02, -0.02, 0, -0.02, -0.02, -0.02, -0.02, -0.02, -0.02],
      "ppi" : [0, 3.6856, 3.6856, 0, 0, 0, 0, 0, 3.6856, 0, 3.6856],
      "h2o" : [-59.276, -59.276, -59.276, 0, 0, -59.276, 0, 0, -59.276, -59.3, -59.276],
      "chitin" : [0, 0, 0, 0, -1.82E-05, 0, 0, 0, 0, 0, -1.00E-06],
      "13BDglcn" : [-1.1348, -1.1348, -1.1348, -1.1348, -1.136, -1.1348, -0.963, -1.1358, -1.1348, -1.14, -1.1348],
      "16BDglcn" : [0, -1.1348, -1.1348, 0, 0, 0, 0, 0, -1.1348, 0, -1.1348],
      "glycogen" : [-0.5185, -0.5185, -0.5185, -0.5185, 0, -0.5185, -0.667, -0.51852, -0.5185, -0.519, -0.5185],
      "mannan" : [-0.8079, -0.8079, -0.8079, -0.8079, -0.809, -0.8079, -0.994, -0.82099, -0.8079, -0.821, -0.8079],
      "tre" : [-0.0234, -0.0234, 0, -0.0234, 0, -0.0234, -0.085, -0.023371, -0.0234, -0.0234, -0.0234],
      "dtmp" : [-0.0036, -0.0036, -0.0036, -0.0036, -0.0036, -0.0036, -0.004, -0.003587, -0.0036, -0.00359, -0.0036],
      "dgmp" : [-0.0024, -0.0024, -0.0024, -0.0024, -0.0024, -0.0024, -0.003, -0.002432, -0.0024, -0.00243, -0.0024],
      "damp" : [-0.0036, -0.0036, -0.0036, -0.0036, -0.0036, -0.0036, -0.004, -0.003587, -0.0036, -0.00359, -0.0036],
      "dcmp" : [-0.0024, -0.0024, -0.0024, -0.0024, -0.0024, -0.0024, -0.003, -0.002432, -0.0024, -0.00243, -0.0024],
      "ump" : [-0.0599, -0.0599, -0.0599, -0.0599, -0.067, -0.0599, -0.052, -0.067, -0.0599, -0.067, -0.0599],
      "gmp" : [-0.046, -0.046, -0.046, -0.046, -0.051, -0.046, -0.04, -0.051, -0.046, -0.051, -0.046],
      "amp" : [-0.046, 3.6396, 3.6396, -0.046, -0.051, -0.046, -0.04, -0.051, 3.6396, -0.051, 3.6396],
      "cmp" : [-0.0447, -0.0447, -0.0447, -0.0447, -0.05, -0.0447, -0.039, -0.05, -0.0447, -0.05, -0.0447],
      "gly" : [-0.2904, -0.2904, -0.2904, -0.2904, -0.29, -0.2904, -0.278, -0.32518, -0.2904, -0.325, -0.2904],
      "ala-L" : [-0.4588, -0.4588, -0.4588, -0.4588, -0.459, -0.4588, -0.252, -0.35734, -0.4588, -0.357, -0.4588],
      "val-L" : [-0.2646, -0.2646, -0.2646, -0.2646, -0.265, -0.2646, -0.18657, -0.25728, -0.2646, -0.257, -0.2646],
      "leu-L" : [-0.2964, -0.2964, -0.2964, -0.2964, -0.296, -0.2964, -0.207, -0.25014, -0.2964, -0.25, -0.2964],
      "ile-L" : [-0.1927, -0.1927, -0.1927, -0.1927, -0.193, -0.1927, -0.142, -0.17152, -0.1927, -0.172, -0.1927],
      "pro-L" : [-0.1647, -0.1647, -0.1647, -0.1647, -0.165, -0.1647, -0.118, -0.12864, -0.1647, -0.129, -0.1647],
      "phe-L" : [-0.1339, -0.1339, -0.1339, -0.1339, -0.134, -0.1339, -0.092, -0.11435, -0.1339, -0.114, -0.1339],
      "tyr-L" : [-0.102, -0.102, -0.102, -0.102, -0.102, -0.102, -0.068, -0.096481, -0.102, -0.0965, -0.102],
      "trp-L" : [-0.0284, -0.0284, -0.0284, -0.0284, -0.028, -0.0284, -0.028, -0.028, -0.0284, -0.028, -0.0284],
      "ser-L" : [-0.1854, -0.1854, -0.1854, -0.1854, -0.185, -0.1854, -0.225, -0.25371, -0.1854, -0.254, -0.1854],
      "thr-L" : [-0.1914, -0.1914, -0.1914, -0.1914, -0.191, -0.1914, -0.16, -0.19653, -0.1914, -0.197, -0.1914],
      "cys-L" : [-0.0066, -0.0066, -0.0066, -0.0066, -0.007, -0.0066, -0.044, -0.04288, -0.0066, -0.0429, -0.0066],
      "met-L" : [-0.0507, -0.0507, -0.0507, -0.0507, -0.051, -0.0507, -0.044, -0.050027, -0.0507, -0.05, -0.0507],
      "asn-L" : [-0.1017, -0.1017, -0.1017, -0.1017, -0.102, -0.1017, -0.153, -0.17152, -0.1017, -0.172, -0.1017],
      "gln-L" : [-0.1054, -0.1054, -0.1054, -0.1054, -0.105, -0.1054, -0.231, -0.268, -0.1054, -0.268, -0.1054],
      "lys-L" : [-0.2862, -0.2862, -0.2862, -0.2862, -0.286, -0.2862, -0.204, -0.23942, -0.2862, -0.239, -0.2862],
      "arg-L" : [-0.1607, -0.1607, -0.1607, -0.1607, -0.161, -0.1607, -0.098, -0.13579, -0.1607, -0.136, -0.1607],
      "his-L" : [-0.0663, -0.0663, -0.0663, -0.0663, -0.066, -0.0663, -0.071, -0.075041, -0.0663, -0.075, -0.0663],
      "asp-L" : [-0.2975, -0.2975, -0.2975, -0.2975, -0.297, -0.2975, -0.153, -0.17152, -0.2975, -0.172, -0.2975],
      "glu-L" : [-0.3018, -0.3018, -0.3018, -0.3018, -0.302, -0.3018, -0.231, -0.268, -0.3018, -0.268, -0.3018],
      "triglyc" : [-6.60E-05, -6.60E-05, -6.60E-05, -0.0066, -7.00E-03, -6.60E-05, -3.62E-03, -7.81E-04, -7.81E-04, -7.81E-04, -7.81E-04],
      "phospholipid (iAZ900 specific)" : [0, -6.00E-05, -6.00E-05, 0, 0, 0, 0, 0, 0, 0, 0],
      "pc" : [-6.00E-05, 0, 0, -0.006, -0.006, -6.00E-05, -0.00166, -0.002884, -0.00288, -0.00288, -0.00288],
      "ps" : [-1.70E-05, -1.70E-05, -1.70E-05, -0.0017, -2.00E-03, -1.70E-05, -3.02E-04, -3.73E-04, -3.90E-04, -3.90E-04, -3.90E-04],
      "pe" : [-4.50E-05, -4.50E-05, -4.50E-05, -0.0045, -2.00E-03, -4.50E-05, -8.30E-05, -6.97E-04, -6.97E-04, -6.97E-04, -6.97E-04],
      "pa" : [-6.00E-06, -6.00E-06, -6.00E-06, -6.00E-04, -6.00E-04, -6.00E-06, 0, 0, 0, 0, 0],
      "ptd1ino" : [-5.30E-05, -5.30E-05, -5.30E-05, -0.0053, -5.00E-03, -5.30E-05, -0.001656, -0.001531, -0.001583, -0.001583, -0.001583],
      "sterol (iAZ900 specific)" : [0, -7.00E-04, -7.00E-04, 0, 0, 0, 0, 0, 0, 0, 0],
      "zymst" : [-0.0015, -0.0015, -0.0015, -0.0015, -0.0157, -0.0015, -1.50E-05, -1.50E-05, -1.50E-05, -1.50E-05, -1.50E-05],
      "epist" : [0, 0, 0, 0, 0, 0, -6.20E-05, -9.60E-05, -9.60E-05, -9.60E-05, -9.60E-05],
      "ergst" : [-7.00E-04, 0, 0, -7.00E-04, 0, -7.00E-04, -0.005155, -0.005603, -0.0056, -0.0056, -0.0056],
      "44mzym" : [0, 0, 0, 0, 0, 0, -4.60E-05, -5.60E-05, -5.60E-05, -5.60E-05, -5.60E-05],
      "fecost" : [0, 0, 0, 0, 0, 0, -6.80E-05, -1.14E-04, -1.14E-04, -1.14E-04, -1.14E-04],
      "ergtetrol" : [0, 0, 0, 0, 0, 0, -1.67E-04, -1.25E-04, -1.25E-04, -1.25E-04, -1.25E-04],
      "lanost" : [0, 0, 0, 0, 0, 0, -7.40E-05, -3.20E-05, -3.20E-05, -3.20E-05, -3.20E-05],
      "ergosterol ester" : [0, 0, 0, 0, 0, 0, -4.63E-03, -8.12E-04, -8.12E-04, -8.12E-04, -8.12E-04],
      "inositol-mannose-P-inositol-P-P-ceramide" : [0, 0, 0, 0, -0.005922, 0, -7.50E-05, -3.51E-04, 0, 0, 0],
      "inositol-mannose-P-inositol-P-D-ceramide" : [0, 0, 0, 0, 0, 0, -9.00E-06, -6.60E-05, 0, 0, 0],
      "sphingolipids" : [0, 0, 0, 0, 0, 0, 0, 0, -4.17E-04, -4.17E-04, -4.17E-04],
      "fatty acids" : [0, 0, 0, 0, 0, 0, -7.23E-04, -2.06E-04, -2.06E-04, -2.06E-04, -2.06E-04],
      "ribflv" : [-9.90E-04, -9.90E-04, -9.90E-04, 0, -1.00E-06, 0, 0, 0, -9.90E-04, -9.00E-04, -9.90E-04],
      "hemeA" : [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1.00E-06],
      "gthrd" : [0, 0, 0, 0, -1.00E-06, 0, 0, 0, 0, 0, 0],
      "qh2" : [0, 0, 0, 0, -1.00E-06, 0, 0, 0, 0, 0, 0],
      "nad" : [0, 0, 0, 0, -1.00E-06, 0, 0, 0, 0, 0, 0],
      "fad" : [0, 0, 0, 0, -1.00E-06, 0, 0, 0, 0, 0, 0],
      "thmtp" : [0, 0, 0, 0, -4.00E-06, 0, 0, 0, 0, 0, 0],
      "camp" : [0, 0, 0, 0, -1.00E-06, 0, 0, 0, 0, 0, 0],
      "coa" : [0, 0, 0, 0, -1.00E-06, 0, 0, 0, 0, 0, 0],
      "thf" : [0, 0, 0, 0, -1.00E-06, 0, 0, 0, 0, 0, 0],
      "pheme" : [0, 0, 0, 0, -1.00E-06, 0, 0, 0, 0, 0, 0]
    };
    $(this.el).html(this.template({models: modelData, species : speciesData}));
    return this;
  }
});

getReactionRef = function(crossref){
    if (crossref.database == 'LigandReaction'){
        return 'http://www.kegg.jp/dbget-bin/www_bget?rn:' + crossref.identifier;
    }
    if (crossref.database == 'MetaCyc') {
        var pgdb = crossref.identifier.split(":");
        return 'http://biocyc.org/' + pgdb[0] + '/NEW-IMAGE?type=REACTION&object=' + pgdb[1];
    }
    if (crossref.database == 'Seed') {
        return 'http://seed-viewer.theseed.org/seedviewer.cgi?page=ReactionViewer&reaction=' + crossref.identifier + '&model=';
    }
    if (crossref.database == 'BiGG') {
        return 'http://bigg.ucsd.edu/universal/reactions/' + crossref.identifier;
    }
}
/*
getReactionRefs = function(crossrefs){
    var links = [];
    for (var i = 0; i < crossrefs.identifiers.length; i++){
        var crossref;
        crossref.database 
        links.push(getReactionRef(crossrefs.identifiers[i]);
    }
}*/

getBrendaRef = function(ecnumber){
    var patt = new RegExp("[0-9-]+\.[0-9-]+\.[0-9-]+\.[0-9-]+");
    var res = patt.test(ecnumber);
    if (res){
        return 'http://www.brenda-enzymes.org/enzyme.php?ecno='+ecnumber;
    } else {
        return null;
    }
}

getLinkGeneGPR = function(gpr, genes){
    gene_map = {};
    for (var i = 0; i < genes.length; i++){
        if (genes[i].sgdgene !== null){
            gene_map[genes[i].leaf.split("@")[0]] = genes[i].sgdgene;
        }
    }
    final_gpr = gpr;
    names = Object.keys(gene_map);
    for (var i = 0; i < names.length; i++){
        var re = new RegExp(names[i], 'g');
        gpr = gpr.replace( re , '<a href="#sgdgenes/' + gene_map[names[i]] + 
                                       '">' + names[i] + '</a >');
    }
    return gpr;
}

getAggregatedCrossReferences = function(crossrefs){
    var dbs = crossrefs.map(function(a){ return a.database; }).unique().remove(null);
    var newCross = [];
    var finalCross = [];
    for (var i = 0; i < dbs.length; i++){
        newCross.push({ database: dbs[i], identifiers: []});
        for (var j = 0; j < crossrefs.length; j++){
            if (crossrefs[j].database == dbs[i]){
                newCross[i].identifiers.push(crossrefs[j].identifier); 
            }
        }
    }
    var maindbs = dbs.map(function(a){ return getDatabaseName(a); }).unique();
    
    for (var i = 0; i < maindbs.length; i++){
        finalCross.push({maindatabase: maindbs[i], crossreferences: []});
        for (var j = 0; j < newCross.length; j++){
            if (maindbs[i] == getDatabaseName(newCross[j].database)){
                finalCross[i].crossreferences.push(newCross[j]);
            }
        }
    }
    return finalCross;
}

getDatabaseName = function(database){
    if (database == "BiGG"){
        return "BiGG";
    } else if (database == "LigandReaction") {
        return "KEGG";
    } else if (database == "ChEBI") {
        return "ChEBI";
    } else if (database == "Seed") {
        return "Seed";
    } else if (database == "LigandDrug"){
        return "KEGG";
    } else if (database == "MetaCyc"){
        return "MetaCyc";
    } else if (database == "LigandCompound") {
        return "KEGG";
    } else if (database == "PubChemSubstance") {
        return "PubChem";
    } else if (database == "LigandGlycan") {
        return "KEGG";
    } else if (database == "PubChemCompound") {
        return "PubChem";
    } else if (database == "HMDB"){
        return "HMDB";
    }
    return null;
}

filterSameGPRs = function(reaction, maingpr, sameGprs){
    var filtered_gprs = [];
    for (var i = 0; i < sameGprs.length; i++){
        if (sameGprs[i].reaction !== reaction){
            var genes = sameGprs[i].gpr.match(/[^(OR)(AND)\s]+[A-Z0-9]+/g);
            genes.sort();
            if (testIfEqualGPR(maingpr, genes)){
                 filtered_gprs.push(sameGprs[i]);
            }
        }
    }
    return filtered_gprs;
}

testIfEqualGPR = function(maingpr, samegpr){
    var main_genes = maingpr.match(/[^(OR)(AND)\s]+[A-Z0-9]+/g);
    main_genes.sort();
    if (main_genes.length !== samegpr.length) return false;
    else {
        for (var i = 0; i < main_genes.length; i++){
            if (main_genes[i] !== samegpr[i]) return false;
        }
    }
    return true;
}

//$(document).ready(function(){
    var app = new AppRouter();
    Backbone.history.start();
//});


